import { createContext, useState, useContext } from "react";
import toast from "react-hot-toast";
import { IProvidersProps } from "../../types";
import { useCharacter } from "../Character";
import api from "./../../services/api";

interface IItemData {
  name?: string;
  weight?: number;
  description: string;
  damage?: number;
  max_ammo?: number;
  ammo?: number;
  attacks?: number;
  range?: number;
  defect?: number;
  area?: number;
  type_id?: number;
}

interface IItem {
  id: number;
  name: string;
  weight: number | null;
  description: string;
  damage: number | null;
  max_ammo: number | null;
  ammo: number | null;
  attacks: number | null;
  range: number | null;
  defect: number | null;
  area: number | null;
  type: string;
}

interface IItemProviderData {
  item: IItem;
  types: ITypes;
  createItem: (itemData: IItemData, charId: number) => void;
  updateItem: (itemData: IItemData, itemId: number) => void;
  deleteItem: (itemId: number, char_id: number) => void;
  getTypes: () => void;
}

interface ITypes {
  [index: number]: { id: number; name: string };
}

const ItemContext = createContext<IItemProviderData>({} as IItemProviderData);

export const ItemProvider = ({ children }: IProvidersProps) => {
  const token = localStorage.getItem("@shuffle:token") || "";
  const [item, setItem] = useState<IItem>({} as IItem);
  const [types, setTypes] = useState<ITypes>({} as ITypes);
  const { readCharacter } = useCharacter();

  const createItem = (itemData: IItemData, charId: number) => {
    api
      .post(`/item/${charId}`, itemData, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((response) => {
        setItem(response.data);
        readCharacter(charId)
        toast.success("Item criado!");
      })
      .catch((err) => {
        toast.error(err.response.data.error);
      });
  };

  const updateItem = (itemData: IItemData, itemId: number) => {
    api
      .patch(`/item/${itemId}`, itemData, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((response) => {
        setItem(response.data);
        toast.success("Item atualizado!");
      })
      .catch((err) => {
        toast.error(err.response.data.error);
      });
  };

  const deleteItem = (itemId: number, char_id: number) => {
    api
      .delete(`/item/${itemId}?char_id=${char_id}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((response) => {
        toast.success("Item deletado!");
        readCharacter(char_id);
      })
      .catch((err) => {
        toast.error(err.response.data.error);
      });
  };

  const getTypes = () => {
    api
      .get("/item/types", {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((response) => {
        setTypes(response.data);
      })
      .catch((err) => {
        toast.error(err.response.data.error);
      });
  };

  return (
    <ItemContext.Provider
      value={{
        item,
        types,
        createItem,
        updateItem,
        deleteItem,
        getTypes,
      }}
    >
      {children}
    </ItemContext.Provider>
  );
};

export const useItem = () => useContext(ItemContext);
