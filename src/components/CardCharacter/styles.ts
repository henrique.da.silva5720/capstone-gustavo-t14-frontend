import styled from "styled-components";

import { IContainerLifeProps, IContainerManaProps } from "./../../types";

export const Container = styled.div`
  width: 300px;

  padding: 5px;

  display: flex;

  border: 2px solid var(--white);
  border-radius: 5px;

  user-select: none;

  figure + div {
    margin-left: 5px;
  }

  :hover {
    cursor: pointer;
  }
`;

export const ContainerImage = styled.figure`
  width: 75px;
  height: 75px;

  border: 2px solid var(--white);
  border-radius: 50%;

  overflow: hidden;

  img {
    width: 130%;

    transform: translateX(-16px);
  }
`;

export const Content = styled.div`
  width: calc(100% - 80px);

  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

export const ContainerLife = styled.div`
  height: 22px;

  background-color: var(--red-dark);
  border-radius: 3px;

  position: relative;

  span {
    position: absolute;
    left: 50%;
    z-index: 1;

    transform: translateX(-50%);
  }
`;

export const Life = styled.div<IContainerLifeProps>`
  width: ${(props) => `${206 * ((props.life * 100) / props.max_life / 100)}px`};
  height: 22px;

  background-color: var(--red);
  border-radius: 3px;

  position: absolute;
  top: 0;
  z-index: 0;

  transition: 0.3s;
`;

export const ContainerMana = styled.div`
  height: 22px;

  background-color: var(--blue-dark);
  border-radius: 3px;

  position: relative;

  span {
    position: absolute;
    left: 50%;
    z-index: 1;

    transform: translateX(-50%);
  }
`;

export const Mana = styled.div<IContainerManaProps>`
  width: ${(props) => `${206 * ((props.mana * 100) / props.max_mana / 100)}px`};
  height: 22px;

  background-color: var(--blue);
  border-radius: 3px;

  position: absolute;
  top: 0;
  z-index: 0;

  transition: 0.3s;
`;

export const ContainerName = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;
