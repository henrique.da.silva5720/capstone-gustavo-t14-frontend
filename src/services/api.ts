import axios from "axios";

const api = axios.create({ baseURL: "https://capstone-shuffle.herokuapp.com" });

export default api;
