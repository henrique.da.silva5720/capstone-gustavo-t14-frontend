import { Background, Container, Form, CloseIcon } from "./styles";

import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

import { useCharacter } from "./../../providers/Character";

import { ICharacterData } from "./../../types";

import { CgCloseO } from 'react-icons/cg'
import { useState } from "react";

interface IModalCharacterProps {
  closeModal: React.Dispatch<React.SetStateAction<boolean>>;
}

const ModalStatus = ({ closeModal }: IModalCharacterProps) => {
  const { character, updateCharacter } = useCharacter();

  const [VMaxLife, setVMaxLife] = useState(character.max_life)
  const [VLife, setVLife] = useState(character.life)
  const [VMaxMana, setVMaxMana] = useState(character.max_mana)
  const [VMana, setVMana] = useState(character.mana)

  const formSchema = yup.object().shape({
		max_life: yup
			.number()
			.required("Required field")
			.transform((value, originalValue) =>
				originalValue === "" ? undefined : value
			),
		life: yup
			.number()
			.required("Required field")
			.transform((value, originalValue) =>
				originalValue === "" ? undefined : value
			),
		max_mana: yup
			.number()
			.required("Required field")
			.transform((value, originalValue) =>
				originalValue === "" ? undefined : value
			),
		mana: yup
			.number()
			.required("Required field")
			.transform((value, originalValue) =>
				originalValue === "" ? undefined : value
			),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({ resolver: yupResolver(formSchema) });

  const handleForm = (data: ICharacterData) => {
		data.max_life = VMaxLife;
		data.life = VLife;
		data.max_mana = VMaxMana;
		data.mana = VMana;
		updateCharacter(character.id, data);

		closeModal(false);
  };

  return (
		<Background>
			<Container>
				<h2>
					Update Status{" "}
					<CloseIcon onClick={() => closeModal(false)}>
						{" "}
						<CgCloseO />{" "}
					</CloseIcon>
				</h2>
				<Form onSubmit={handleSubmit(handleForm)}>
					<div>
						<div>
							<label>
								Max Life
								{!!errors.max_life && (
									<span> - {errors.max_life?.message}</span>
								)}
							</label>
							<input
								type="number"
								{...register("max_life")}
								value={VMaxLife}
								onChange={(e) =>
									
									setVMaxLife(Number(e.target.value))
								}
							/>
						</div>
						<div>
							<label>
								Life{" "}
								{!!errors.life && (
									<span>- {errors.life?.message}</span>
								)}
							</label>
							<input
								type="number"
								{...register("life")}
								value={VLife}
								onChange={(e) => {
									if (Number(e.target.value) > VMaxLife){
										return;
									}
									setVLife(Number(e.target.value))}
								}
							/>
						</div>
					</div>
					<div>
						<div>
							<label>
								Max Mana
								{!!errors.max_mana && (
									<span> - {errors.max_mana?.message}</span>
								)}
							</label>
							<input
								type="number"
								{...register("max_mana")}
								value={VMaxMana}
								onChange={(e) =>
									setVMaxMana(Number(e.target.value))
								}
							/>
						</div>
						<div>
							<label>
								Mana{" "}
								{!!errors.mana && (
									<span>- {errors.mana?.message}</span>
								)}
							</label>
							<input
								type="number"
								{...register("mana")}
								value={VMana}
								onChange={(e) => {
									if (Number(e.target.value) > VMaxMana) {
										return;
									};
									setVMana(Number(e.target.value))}
								}
							/>
						</div>
					</div>

					<button type="submit">Update</button>
				</Form>
			</Container>
		</Background>
  );
};

export default ModalStatus;
