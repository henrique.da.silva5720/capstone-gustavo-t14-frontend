import styled from "styled-components";

export const Container = styled.section`
  width: 100%;
  margin: 1rem;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  gap: 5px;
  margin: 0;
`;

export const TitleHold = styled.div`
  display: flex;
  width: 90%;
  border-bottom: 3px solid var(--white);
  justify-content: space-between;
  align-items: center;
  margin-bottom: 1rem;
`;
export const Title = styled.h1`
  text-align: left;
  margin-bottom: 1rem;
  padding: 0.5rem 2rem;
`;

export const AddBtn = styled.button`
  background-color: #e3e3e3;
  border: none;
  padding: 0.5rem 1rem;
  font-weight: bold;
  font-size: 1rem;
  display: flex;
  align-items: center;
  gap: 5px;
`;

export const CharsList = styled.div`
  display: flex;
  gap: 10px;
  flex-wrap: wrap;
  align-items: center;
  justify-content: center;
`;

export const WarningMsg = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;
