import styled from "styled-components";

export const Container = styled.section`
  width: 100%;
  margin: 1rem;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  gap: 5px;
  margin: 0;
`;
export const Title = styled.h1`
  border-bottom: 3px solid var(--white);
  width: 90%;
  text-align: left;
  margin: 1rem;
  padding: 0.5rem 2rem;
`;
