import { Maincontainer, Contentcontainer, Line, LogoContainer } from "./styles";

import { SignupForm } from "../../components/SignupForm";

import { LoginForm } from "../../components/LoginForm";
import { useUser } from "../../providers/User";
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";

const HomePage = () => {
  const { auth } = useUser();
  const navigate = useNavigate();

  useEffect(() => {
    if (auth) {
      navigate("/dashboard");
    }
  });

  return (
    <>
      <Maincontainer>
        <Contentcontainer>
          <SignupForm />
          <Line />
          <LogoContainer>
            <img
              src={"https://i.imgur.com/hYjS2es.png"}
              className="logo"
              alt="dice"
            />
          </LogoContainer>
          <LoginForm />
        </Contentcontainer>
      </Maincontainer>
    </>
  );
};

export default HomePage;
