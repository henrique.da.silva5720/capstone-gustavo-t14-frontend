import styled from "styled-components";

export const Background = styled.div`
  width: 100vw;
  height: 100vh;

  display: flex;
  justify-content: center;
  align-items: center;

  background-color: rgba(0, 0, 0, 0.5);
  backdrop-filter: blur(4px);

  position: fixed;
  z-index: 3;
  top: 0;
`;

export const Container = styled.div`
  width: 550px;
  max-height: 95%;
  display: flex;
  flex-direction: column;
  overflow: auto;
  background-color: var(--black);
  border: 2px solid var(--white);
  border-radius: 10px;

  h2 {
    text-align: center;
    font-size: 2.3rem;
    margin-top: 1rem;
    position: relative;
  }
`;

export const Form = styled.form`
  width: 360px;

  margin: 20px auto 0;
  padding-bottom: 20px;

  display: flex;
  flex-direction: column;
  

  label span {
    font-size: 0.8rem;
    color: var(--red-light);
  }

  input,
  button,
  select {
    width: 360px;
    background-color: var(--white);
    padding: 5px 10px;

    font-size: 1rem;

    border: none;
    border-radius: 5px;
  }

  button {
    width: 50%;
    height: 40px;

    margin-top: 20px;

    display: flex;
    justify-content: center;
    align-items: center;
    align-self: center;

    color: var(--white);

    background-color: var(--blue-light);
  }

  > div {
    display: flex;
    

    input {
      width: 170px;
    }

  }
`;

export const Image = styled.div`
  cursor: pointer;
  position: absolute;
  top: 0;
  right: 1rem;
`;

export const BreakableDiv = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  gap: 20px;
  margin: 1.2rem 0 .3rem;
`

export const InnerDivs = styled.div`
  display: flex;
  gap: 15px;
  justify-content: end;
  align-items: center;

  input[type=number] {
    width: 90px;
  }
`