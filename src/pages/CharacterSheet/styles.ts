import styled from "styled-components";

import { IContainerLifeProps, IContainerManaProps } from "./../../types";

export const Container = styled.main`
  max-width: 1440px;

  margin: 50px auto;

  > div {
    display: flex;
  }
`;

export const ContainerCharacter = styled.article`
  width: 480px;

  display: flex;
  flex-direction: column;
  align-items: center;

  h1 {
    margin-bottom: 20px;

    font-size: 2.2rem;
  }

  > div {
    display: flex;

    figure {
      width: 180px;

      img {
        width: 100%;
      }
    }

    > div {
      width: 250px;

      margin-left: 50px;
    }
  }
`;

export const ContainerStatus = styled.div`
  margin-bottom: 30px;

  font-size: 1.2rem;
`;

export const ContainerLife = styled.div`
  height: 32px;

  margin-bottom: 10px;

  display: flex;

  background-color: var(--red-dark);
  border-radius: 3px;

  position: relative;
  cursor: pointer;

  span {
    position: absolute;
    top: 50%;
    left: 50%;
    z-index: 1;

    transform: translate(-50%, -50%);
  }
`;

export const Life = styled.div<IContainerLifeProps>`
  width: ${(props) => `${250 * ((props.life * 100) / props.max_life / 100)}px`};
  height: 32px;

  background-color: var(--red);
  border-radius: 3px;

  position: absolute;
  top: 0;
  z-index: 0;

  transition: 0.3s;
`;

export const ContainerMana = styled.div`
	height: 32px;

	display: flex;

	background-color: var(--blue-dark);
	border-radius: 3px;

	position: relative;
	cursor: pointer;

	span {
		position: absolute;
		top: 50%;
		left: 50%;
		z-index: 1;

		transform: translate(-50%, -50%);
	}
`;

export const Mana = styled.div<IContainerManaProps>`
  width: ${(props) => `${250 * ((props.mana * 100) / props.max_mana / 100)}px`};
  height: 32px;

  background-color: var(--blue);
  border-radius: 3px;

  position: absolute;
  top: 0;
  z-index: 0;

  transition: 0.3s;
`;

export const ContainerAppearance = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const Appearance = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  span:nth-child(1) {
    font-size: 1rem;
  }

  span:nth-child(2) {
    margin-bottom: 5px;

    font-size: 2rem;
  }

  hr {
    width: 75px;
  }
`;

export const ContainerData = styled.div`
  margin-top: 30px;
  padding: 10px;

  border: 2px solid var(--white);
  border-radius: 10px;
`;

export const Data = styled.div`
  display: flex;
  flex-direction: column;

  span:nth-child(1) {
    font-size: 1.2rem;
    font-weight: bold;
  }

  span:nth-child(2) {
    margin-left: 5px;
  }
`;

export const ContainerAttributes = styled.article`
  width: calc(1440px - 480px - 50px);
  height: 197px;

  margin-left: 50px;
  padding: 10px;

  border: 2px solid var(--white);
  border-radius: 10px;

  h2 {
    margin-bottom: 5px;

    text-align: center;
    font-size: 2.2rem;
  }

  > div {
    display: flex;
    justify-content: space-between;
  }
`;

export const ContainerSkills = styled.article`
  width: calc(1440px - 480px - 50px);
  height: 367px;
  position: relative;
  margin: 50px 0 0 50px;
  padding: 10px;

  border: 2px solid var(--white);
  border-radius: 10px;

  overflow-y: auto;

  h2 {
    margin-bottom: 5px;

    text-align: center;
    font-size: 2.2rem;
  }

  > div {
    display: flex;
    flex-wrap: wrap;

    div {
      margin-bottom: 10px;
    }

    div + div {
      margin-left: 0.8px;
    }
  }

  ::-webkit-scrollbar {
    width: 5px;

    position: absolute;
  }

  :hover::-webkit-scrollbar-thumb {
    background-color: var(--white);
    border-radius: 5px;
  }
`;

export const ContainerCombat = styled.article`
	min-width: 1440px;
	height: 310px;
	overflow: auto;
	position: relative;

	margin-top: 50px;
	padding: 0 10px;

	border: 2px solid var(--white);
	border-radius: 10px;

	h2 {
		background-color: var(--black);
		margin-bottom: 5px;

		text-align: center;
		font-size: 2.2rem;

		position: sticky;
		top: 0;
	}

	div {
		padding: 0 20px;
		display: flex;
		font-size: 1.5rem;

		/* span {
			flex-basis: 140px;
		} */
	}

	hr {
		margin: 10px 0;
	}
`;

export const TableTitle = styled.span`
  flex-basis: 140px;
`
export const TableDel = styled.span`
  flex-basis: 50px;
  display: flex;
  align-items: center;
  cursor: pointer;
`

export const ContainerInventory = styled.article`
  min-width: 1440px;
  height: 310px;
  overflow: auto;
  position: relative;

  margin-top: 50px;
  padding: 0 10px;

  border: 2px solid var(--white);
  border-radius: 10px;

  h2 {
  background-color: var(--black);
    margin-bottom: 5px;

    text-align: center;
    font-size: 2.2rem;

    position: sticky;
    top: 0;
  }


  div {
    padding: 0 20px;
    display: flex;
    font-size: 1.5rem;

  /* span {
    flex-basis: 250px;
  } */
}

  hr {
    margin: 10px 0;
  }
`;

export const ItemTableTitle = styled.span`
  flex-basis: 250px;
`

export const Item = styled.div`
	padding: 10px 20px;
	display: flex;
  max-width: 600px;

`;
export const ItemInfos = styled.span`
	font-size: 1.2rem;
	flex-basis: 250px;
	padding: 0.5rem 0;
`;

export const Weapon = styled.div`
  min-width: 100%;
	padding: 10px 20px;
	display: flex;
	max-width: 600px;

`;
export const WeaponInfos = styled.span`
	font-size: 1.2rem;
	flex-basis: 140px;
	padding: 0.5rem 0;
`;

export const SkillButton = styled.button`
	position: absolute;
	top: 1rem;
	right: 1rem;
	background-color: #e3e3e3;
	border: none;
	padding: 0.5rem 1rem;
	font-weight: bold;
	font-size: 1rem;
	display: flex;
	align-items: center;
	gap: 5px;
`;

export const AddItemBtn = styled.button`
	position: absolute;
	right: 1rem;
	top: 0rem;
	background-color: #e3e3e3;
	border: none;
	padding: 0.5rem 1rem;
	font-weight: bold;
	font-size: 1rem;
	display: flex;
	align-items: center;
	gap: 5px;
`;

export const ItemsContainer = styled.div`
  display: flex;
  flex-direction: column;
`