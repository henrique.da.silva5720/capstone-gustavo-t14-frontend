import { createContext, useContext } from "react";
import toast from "react-hot-toast";
import { IProvidersProps } from "../../types";
import { useCharacter } from "../Character";
import { useUser } from "../User";
import api from "./../../services/api";

interface ICreateSkill {
  charId: number;
  skillName: string;
}
interface IUpdateSkill {
  charId: number;
  skillId: number;
  value: number;
}
interface IDeleteSkill {
  charId: number;
  skillId: number;
}
interface ISkillProps {
  createSkill: (a: number, b: string) => void;
  updateSkill: (a: IUpdateSkill) => void;
  deleteSkill: (a: IDeleteSkill) => void;
}

const SkillContext = createContext({} as ISkillProps);

export const SkillProvider = ({ children }: IProvidersProps) => {
  const { auth } = useUser();
  const { readCharacter } = useCharacter()

  const createSkill = (charId: number, skillName: string) => {
    api
      .post(
        `/skill/${charId}`,
        { name: skillName },
        {
          headers: { Authorization: `Bearer ${auth}` },
        }
      )
      .then((res) => {toast.success(`Skill ${skillName} criada!`); readCharacter(charId)})
      .catch((err) => toast.error(err.error));
  };

  const updateSkill = ({ charId, skillId, value }: IUpdateSkill) => {
    api
      .patch(
        `/skill/${skillId}?char_id=${charId}`,
        { value: value },
        {
          headers: { Authorization: `Bearer ${auth}` },
        }
      )
      .then((res) => {
        readCharacter(charId);
        toast.success("Skill atualizada!");
      })
      .catch((err) => toast.error(err.error));
  };

  const deleteSkill = ({ charId, skillId }: IDeleteSkill) => {
    api
      .delete(`/skill/${skillId}?char_id=${charId}`)
      .then((res) => toast.success(res.data.msg))
      .catch((err) => toast.error(err.error));
  };

  return (
    <SkillContext.Provider
      value={{
        createSkill,
        updateSkill,
        deleteSkill,
      }}
    >
      {children}
    </SkillContext.Provider>
  );
};

export const useSkill = () => useContext(SkillContext);
