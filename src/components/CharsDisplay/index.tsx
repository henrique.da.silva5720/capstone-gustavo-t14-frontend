import {
  Container,
  Title,
  TitleHold,
  AddBtn,
  CharsList,
  WarningMsg,
} from "./styles";
import { useCharacter } from "../../providers/Character";
import CardCharacter from "../CardCharacter";
import { TiPlusOutline } from "react-icons/ti";
import ModalCharacter from "./../ModalCharacter";
import { useEffect, useState } from "react";

const CharsDisplay = () => {
  const { allCharacters, setReload, reload } = useCharacter();

  const [showModal, setShowModal] = useState<boolean>(false);

  useEffect(() => {
    setReload(!reload)
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])
  return (
    <Container>
      <TitleHold>
        <Title>Personagens</Title>
        <AddBtn onClick={() => setShowModal(true)}>
          <TiPlusOutline />
          Novo
        </AddBtn>
      </TitleHold>
      <CharsList>
        {allCharacters.length ? (
          allCharacters.map((char) => (
            <CardCharacter character={char} key={char.id} />
          ))
        ) : (
          <WarningMsg>
            <h1> Você ainda não tem nenhum personagem!</h1>
            <h3>
              Clique em <span>Novo</span> para criar o primeiro...
            </h3>
          </WarningMsg>
        )}
      </CharsList>
      {showModal && <ModalCharacter closeModal={setShowModal} />}
    </Container>
  );
};

export default CharsDisplay;
