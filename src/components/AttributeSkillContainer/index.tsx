import { Container, Line, Title, NumberStyle, Dice, Change } from "./styles";

import dice from "../../assets/images/dice.svg";

import { useState } from "react";

import { useAttributes } from "../../providers/Attributes";
import { useSkill } from "../../providers/Skill";
import { useCharacter } from "../../providers/Character";

interface IAttributes {
  id?: number;
  name: string;
  value: number;
}
interface ISkills {
  id: number;
  name: string;
  value: number;
}

interface IAttributeSkill {
  skill?: boolean;
  obj: ISkills | IAttributes;
  character_id: number;
}

interface IUpdateSkill {
  charId: number;
  skillId: number;
  value: number;
}

const AttributeSkillContainer = ({
  skill = false,
  obj,
  character_id,
}: IAttributeSkill) => {
  const [update, setUpdate] = useState<boolean>(false);
  const [value, setValue] = useState<string>("");
  const { updateSkill } = useSkill();
  const { updateAttributes } = useAttributes();
  
  const attAtrributeSkill = (event: string) => {
    if (event === "Enter") {
      setUpdate(!update);
      if (skill) {
        const data: IUpdateSkill = {
          charId: character_id,
          skillId: obj.id || 0,
          value: Number(value),
        };
        updateSkill(data);
      } else {
        const data = Object.create({});
        data[obj.name] = Number(value);
        updateAttributes(data, character_id);
      }
    }
  };
  return (
    <Container>
      <Title>{obj.name}</Title>
      {update ? (
        <Change
          type={"number"}
          onChange={(e) => setValue(e.target.value)}
          onKeyPress={(e) => attAtrributeSkill(e.key)}
        />
      ) : (
        <NumberStyle onClick={() => setUpdate(!update)}>
          {obj.value}
        </NumberStyle>
      )}
      <Line />
      <Dice src={dice} />
    </Container>
  );
};

export default AttributeSkillContainer;
