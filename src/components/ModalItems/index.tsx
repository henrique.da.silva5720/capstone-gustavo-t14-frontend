import {
	Container,
	Background,
	Form,
	Image,
	BreakableDiv,
	InnerDivs,
} from "./style";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { useItem } from "../../providers/Items";
import { CgCloseO } from "react-icons/cg";

interface IItemData {
  name?: string;
  weight?: number;
  description: string;
  damage?: number;
  max_ammo?: number;
  ammo?: number;
  attacks?: number;
  range?: number;
  defect?: number;
  area?: number;
  type_id?: number;
}

interface IModalItemsProps {
  closeModal: React.Dispatch<React.SetStateAction<boolean>>;
  char_id: number;
}

const ModalItems = ({ closeModal, char_id }: IModalItemsProps) => {
  const { createItem } = useItem();

  const formSchema = yup.object().shape({
    name: yup.string().required("Required field"),
    weight: yup
      .number()
      .transform((value, originalValue) =>
        originalValue === "" ? undefined : value
      ),
    description: yup.string().required("Required field"),
    damage: yup
      .string()
      .transform((value, originalValue) =>
        originalValue === "" ? undefined : value
      ),
    max_ammo: yup
      .number()
      .transform((value, originalValue) =>
        originalValue === "" ? undefined : value
      ),
    ammo: yup
      .number()
      .transform((value, originalValue) =>
        originalValue === "" ? undefined : value
      ),
    attacks: yup
      .string()
      .transform((value, originalValue) =>
        originalValue === "" ? undefined : value
      ),
    range: yup
      .number()
      .transform((value, originalValue) =>
        originalValue === "" ? undefined : value
      ),
    defect: yup
      .number()
      .transform((value, originalValue) =>
        originalValue === "" ? undefined : value
      ),
    area: yup
      .number()
      .transform((value, originalValue) =>
        originalValue === "" ? undefined : value
      ),
    type_id: yup.string().required(),
  });

  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm({ resolver: yupResolver(formSchema) });

  const handleForm = (data: IItemData) => {
    data.type_id = Number(data.type_id);
    createItem(data, char_id);
    closeModal(false);
  };

  return (
    <Background>
      <Container>
        
        <h2>Create Item<Image onClick={() => closeModal(false)}>
          <CgCloseO />
        </Image></h2>
        <Form onSubmit={handleSubmit(handleForm)}>
          <label>
            Name * {!!errors.name && <span>- {errors.name?.message}</span>}
          </label>
          <input {...register("name")} />
          <label>
            Weight {!!errors.weight && <span>- {errors.weight?.message}</span>}
          </label>
          <input type="number" {...register("weight")} />
          <label>
            Description *{" "}
            {!!errors.description && (
              <span>- {errors.description?.message}</span>
            )}
          </label>
          <input {...register("description")} />
          <label>
            Damage
            {!!errors.damage && <span> - {errors.damage?.message}</span>}
          </label>
          <input {...register("damage")} />
          <BreakableDiv>
            <InnerDivs>
              <label>
                Max_ammo
                {!!errors.max_ammo && (
                  <span> - {errors.max_ammo?.message}</span>
                )}
              </label>
              <input type="number" {...register("max_ammo")} />
            </InnerDivs>
            <InnerDivs>
              <label>
                Ammo {!!errors.ammo && <span>- {errors.ammo?.message}</span>}
              </label>
              <input type="number" {...register("ammo")} />
            </InnerDivs>
          </BreakableDiv>

          <label>
            Attacks
            {!!errors.attacks && <span> - {errors.attacks?.message}</span>}
          </label>
          <input {...register("attacks")} />

          <label>
            Range {!!errors.range && <span>- {errors.range?.message}</span>}
          </label>
          <input type="number" {...register("range")} />

          <label>
            Defect {!!errors.defect && <span>- {errors.defect?.message}</span>}
          </label>
          <input type="number" step="0.01" {...register("defect")} />
          <label>
            Area {!!errors.area && <span>- {errors.area?.message}</span>}
          </label>
          <input type="number" {...register("area")} />
          <label>
            Type *
            {!!errors.type_id && <span> - {errors.type_id?.message}</span>}
          </label>
          <select
            {...register("type_id")}
            onChange={(e) =>
              setValue("type_id", e.target.value, { shouldValidate: true })
            }
          >
            <option></option>
            <option value={1}>Standart</option>
            <option value={2}>Key</option>
            <option value={3}>Combat</option>
            <option value={4}>Medicine</option>
          </select>

          <button type="submit">Create</button>
        </Form>
      </Container>
    </Background>
  );
};

export default ModalItems;
