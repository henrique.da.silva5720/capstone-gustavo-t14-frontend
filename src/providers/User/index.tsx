import { createContext, useState, useContext, useEffect } from "react";
import toast from "react-hot-toast";
import { NavigateFunction } from "react-router-dom";
import { IProvidersProps } from "../../types";
import api from "./../../services/api";
import jwt_decode, { JwtPayload } from "jwt-decode";

interface ISignupData {
  name: string;
  email: string;
  password: string;
}

interface ILoginData {
  email: string;
  password: string;
}

interface IUser {
  id: number;
  name: string;
  email: string;
}

interface IUserProps {
  auth: string;
  signup: (a: ISignupData, b: NavigateFunction) => void;
  login: (a: ILoginData, b: NavigateFunction) => void;
  user: IUser;
  setAuth: (a: string) => void;
}

const UserContext = createContext({} as IUserProps);

export const UserProvider = ({ children }: IProvidersProps) => {
  let token = localStorage.getItem("@shuffle:token") || "";
  const [auth, setAuth] = useState<string>(token);
  const [user, setUser] = useState<IUser>({} as IUser);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(() => {
    if (auth) {
      const decoded: JwtPayload = jwt_decode(auth);
      const expDate: number = decoded.exp!!;
      const atualDate = new Date().getTime();

      if (expDate * 1000 < atualDate) {
        setAuth("");
        localStorage.removeItem("@shuffle:token");
        toast.error("JWT expirou! Faça login novamente!");
        window.location.href = "/";
      }
    }
    if (auth && !user.name) {
      api
        .get("/user", {
          headers: { Authorization: `Bearer ${auth}` },
        })
        .then((response) => {
          setUser(response.data);
        })
        .catch((err) => {
          console.log(err);
          toast.error(err.response.data.error);
        });
    }
  });
  const signup = (userData: ISignupData, navigate: NavigateFunction) => {
    api
      .post("/user/signup", userData)
      .then((response) => {
        localStorage.setItem("@shuffle:token", response.data.access_token);
        setAuth(response.data.access_token);
        toast.success("Bem vindo ao Shuffle!");
        api
          .get("/user", {
            headers: { Authorization: `Bearer ${response.data.access_token}` },
          })
          .then((response) => {
            setUser(response.data);
            navigate("/dashboard");
          })
          .catch((err) => {
            console.log(err);
            toast.error(err.response.data.error);
          });
      })
      .catch((err) => {
        console.log(err);
        toast.error(err.response.data.error);
      });
  };

  const login = (userData: ILoginData, navigate: NavigateFunction) => {
    api
      .post("/user/login", userData)
      .then((response) => {
        localStorage.setItem("@shuffle:token", response.data.access_token);
        setAuth(response.data.access_token);
        toast.success("Bem vindo ao Shuffle!");
        api
          .get("/user", {
            headers: { Authorization: `Bearer ${response.data.access_token}` },
          })
          .then((response) => {
            setUser(response.data);
            navigate("/dashboard");
          })
          .catch((err) => {
            console.log(err);
            toast.error(err.response.data.error);
          });
      })
      .catch((err) => {
        console.log(err);
        toast.error(err.response.data.error);
      });
  };

  return (
    <UserContext.Provider
      value={{
        user,
        signup,
        login,
        auth,
        setAuth,
      }}
    >
      {children}
    </UserContext.Provider>
  );
};

export const useUser = () => useContext(UserContext);
