import {
  Title,
  SignupContainer,
  FormBlock,
  Block,
  UniqueBlock,
  LabelBox,
  BlueButton,
} from "./styles";
import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";

import { useUser } from "../../providers/User";
import { useNavigate } from "react-router-dom";

export const SignupForm = () => {
  const { signup } = useUser();
  const navigate = useNavigate();

  type FormValues = {
    name: string;
    email: string;
    password: string;
    verifypassword: string;
  };

  const formSchema = yup.object().shape({
    name: yup
      .string()
      .required("Nome obrigatorio")
      .matches(/^[a-zA-Z\s]+$/, "Digite um nome com apenas letras"),
    email: yup
      .string()
      .required("email obrigatorio")
      .matches(
        /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
        "formato de email invalido"
      ),

    password: yup
      .string()
      .required("Campo obrigatório")
      .min(8, "Minimo 8 caractéres")
      .matches(
        /^.*((?=.*[!@#$%^&*]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/,
        "Requer: letra maiúscula, minúscula, número, caracter especial"
      ),
    verifyPassword: yup
      .string()
      .required("Campo obrigatório")
      .oneOf([yup.ref("password")], "As Senhas não correspondem"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(formSchema),
  });

  const requisition = (data: FormValues) => {
    const { name, email, password } = data;
    const new_object = { name: name, email: email, password: password };
    signup(new_object, navigate);
  };
  return (
    <>
      <SignupContainer>
        <Title>Sign Up</Title>
        <FormBlock onSubmit={handleSubmit(requisition)}>
          <LabelBox>Username:</LabelBox>
          <input {...register("name")} className="username" />
          {errors.name?.message}
          <LabelBox>Email:</LabelBox>
          <input {...register("email")} className="email" />
          {errors.email?.message}
          <Block>
            <UniqueBlock>
              <label className="lPass">Password:</label>
              <input {...register("password")} className="password" />
              {errors.password?.message}
            </UniqueBlock>
            <UniqueBlock>
              <label className="CPass">Confirm Password:</label>
              <input {...register("verifyPassword")} className="conpassword" />
              {errors.verifyPassword?.message}
            </UniqueBlock>
          </Block>
          <BlueButton type="submit">Submit</BlueButton>
        </FormBlock>
      </SignupContainer>
    </>
  );
};
