import GlobalStyle from "./styles/global";

import { Toaster } from "react-hot-toast";

import Router from "./routes";

const App = () => {
  return (
    <>
      <GlobalStyle />
      <Toaster />
      <Router />
    </>
  );
};

export default App;
