import {
  Container,
  ContainerImage,
  Content,
  ContainerLife,
  Life,
  ContainerMana,
  Mana,
  ContainerName,
} from "./styles";

import { ICharacters } from "./../../types";

import { useNavigate } from "react-router-dom";

import { useCharacter } from "./../../providers/Character";

import { RiDeleteBin5Line } from "react-icons/ri";

interface ICardCharacterProps {
  character: ICharacters;
}

const CardCharacter = ({ character }: ICardCharacterProps) => {
  const navigate = useNavigate();

  const { readCharacter, deleteCharacter } = useCharacter();

  return (
    <Container
      onClick={() => {
        readCharacter(character.id);
        navigate(`/character_sheet/${character.id}`);
      }}
    >
      <ContainerImage>
        <img src={character.image_url} alt="char_img" />
      </ContainerImage>
      <Content>
        <ContainerName>
          <span>{character.name}</span>
          <RiDeleteBin5Line
            onClick={(e) => {
              e.stopPropagation();
              deleteCharacter(character.id);
            }}
          />
        </ContainerName>
        <ContainerLife>
          <span>
            {character.life} / {character.max_life}
          </span>
          <Life max_life={character.max_life} life={character.life} />
        </ContainerLife>
        <ContainerMana>
          <span>
            {character.mana} / {character.max_mana}
          </span>
          <Mana max_mana={character.max_mana} mana={character.mana} />
        </ContainerMana>
      </Content>
    </Container>
  );
};

export default CardCharacter;
