import styled from "styled-components";

export const Background = styled.div`
  width: 100vw;
  height: 100vh;

  display: flex;
  justify-content: center;
  align-items: center;

  background-color: rgba(0, 0, 0, 0.5);
  backdrop-filter: blur(4px);

  position: fixed;
  z-index: 3;
  top: 0;
`;

export const Container = styled.div`
  width: 500px;

  padding: 20px;

  background-color: var(--black);
  border: 2px solid var(--white);
  border-radius: 10px;

  h2 {
    text-align: center;
    font-size: 2.3rem;
    position: relative;
  }
`;

export const Form = styled.form`
  width: 360px;

  margin: 20px auto 0;

  display: flex;
  flex-direction: column;

  label span {
    font-size: 0.8rem;
    color: var(--red-light);
  }

  input,
  button {
    width: 360px;

    padding: 5px 10px;

    font-size: 1rem;

    border: none;
    border-radius: 5px;
  }

  button {
    width: 50%;
    height: 40px;

    margin-top: 20px;

    display: flex;
    justify-content: center;
    align-items: center;
    align-self: center;

    color: var(--white);

    background-color: var(--blue-light);
  }

  > div {
    display: flex;

    input {
      width: 170px;
    }

    div + div {
      margin-left: 10px;
    }
  }
`;

export const CloseIcon = styled.div`
  position: absolute;
  right: -10px;
  top: -10px;
  cursor: pointer;
`