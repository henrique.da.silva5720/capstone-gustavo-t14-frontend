import { useLocation, Navigate } from "react-router-dom";
import { useUser } from "../providers/User";

function RequireAuth({ children }: { children: JSX.Element }) {
  const { auth } = useUser();
  let location = useLocation();

  if (!auth) {
    return <Navigate to="/" state={{ from: location }} />;
  }

  return children;
}

export default RequireAuth;
