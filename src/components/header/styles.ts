import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 30px;
  box-shadow: 0px 10px 50px rgba(255, 255, 255, 0.25);
  margin-bottom: 1rem;
`;
export const UserName = styled.span`
  font-size: 1rem;
`;

export const Logo = styled.img`
  width: 50px;
  cursor: pointer;
`;

export const Logout = styled.a`
  font-size: 1rem;
  display: flex;
  align-items: center;
  gap: 3px;
`;
