import { ReactNode } from "react";

export interface IProvidersProps {
  children: ReactNode;
}

export interface ICharacters {
  id: number;
  name: string;
  max_life: number;
  life: number;
  max_mana: number;
  mana: number;
  image_url: string;
}

export interface ICharacterData {
  name: string;
  age?: number;
  gender?: string;
  occupation?: string;
  max_life: number;
  life: number;
  max_mana: number;
  mana: number;
  height?: number;
  body?: number;
  moviment?: number;
}

export interface IContainerLifeProps {
  max_life: number;
  life: number;
}

export interface IContainerManaProps {
  max_mana: number;
  mana: number;
}
