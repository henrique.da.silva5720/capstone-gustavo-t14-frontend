import toast from "react-hot-toast";

import { createContext, useContext, useState, useEffect } from "react";

import { IProvidersProps, ICharacters, ICharacterData } from "./../../types";

import api from "./../../services/api";
import { useUser } from "../User";

interface IAttributes {
  name: string;
  value: number;
}
interface ISkills {
  id: number;
  name: string;
  value: number;
}
interface IItems {
  id: number;
  name: string;
  weight?: number | null;
  description: string;
  damage?: number | null;
  max_ammo?: number | null;
  ammo?: number | null;
  attacks?: number | null;
  range?: number | null;
  defect?: number | null;
  area?: number | null;
  type: string;
}

interface ICharacter {
  id: number;
  name: string;
  age: number | null;
  gender: string | null;
  occupation: string | null;
  max_life: number;
  life: number;
  max_mana: number;
  mana: number;
  height: number | null;
  body: number | null;
  moviment: number | null;
  image_url: string;
  attributes: IAttributes[];
  skills: ISkills[];
  items: IItems[];
}

interface IUpdate {
  max_life?: number;
  life?: number;
  max_mana?: number;
  mana?: number;
}

interface ICharacterProviderData {
  allCharacters: ICharacters[];
  character: ICharacter;
  createCharacter: (characterData: ICharacterData) => void;
  readCharacter: (cid: number) => void;
  updateCharacter: (
    charId: number,
    characterData: IUpdate
  ) => void;
  deleteCharacter: (id: number) => void;
  reload: boolean;
  setReload: (a: boolean) => void;
}

const CharacterContext = createContext<ICharacterProviderData>(
  {} as ICharacterProviderData
);

export const CharacterProvider = ({ children }: IProvidersProps) => {
  const { auth } = useUser();
  const [allCharacters, setAllCharacters] = useState<ICharacters[]>([]);
  const [character, setCharacter] = useState<ICharacter>({} as ICharacter);
  const [reload, setReload] = useState(false);

  const createCharacter = (characterData: ICharacterData) => {
    api
      .post("/character", characterData, {
        headers: { Authorization: `Bearer ${auth}` },
      })
      .then((_) => {
        setReload(!reload);
        toast.success("Successfully created character sheet");
      })
      .catch((err) => {
        console.log(err);
        !!err.response && toast.error(err.response.data.error);
      });
  };

  const readCharacter = (id: number) => {
    api
      .get(`/character/${id}`, {
        headers: { Authorization: `Bearer ${auth}` },
      })
      .then((response) => setCharacter(response.data))
      .catch((err) => {
        console.log(err);
        !!err.response && toast.error(err.response.data.error);
      });
  };

  const updateCharacter = (
    charId: number,
    characterData: IUpdate
  ) => {
    api
      .patch(`/character/${charId}`, characterData, {
        headers: { Authorization: `Bearer ${auth}` },
      })
      .then((_) => {toast.success("Character data successfully updated"); readCharacter(charId)})
      .catch((err) => {
        console.log(err);
        !!err.response && toast.error(err.response.data.error);
      });
  };

  const deleteCharacter = (id: number) => {
    api
      .delete(`/character/${id}`, {
        headers: { Authorization: `Bearer ${auth}` },
      })
      .then((response) => {
        setReload(!reload);
        toast.success(response.data.message);
      })
      .catch((err) => {
        console.log(err);
        !!err.response && toast.error(err.response.data.error);
      });
  };

  useEffect(() => {
    if (!!auth) {
      api
        .get("/character", {
          headers: { Authorization: `Bearer ${auth}` },
        })
        .then((response) => setAllCharacters(response.data))
        .catch((err) => {
          console.log(err);
          !!err.response && toast.error(err.response.data.error);
        });
    }

  }, [auth, reload]);

  return (
    <CharacterContext.Provider
      value={{
        allCharacters,
        character,
        createCharacter,
        readCharacter,
        updateCharacter,
        deleteCharacter,
        reload,
        setReload
      }}
    >
      {children}
    </CharacterContext.Provider>
  );
};

export const useCharacter = () => useContext(CharacterContext);
