import { useNavigate } from "react-router-dom";
import { useUser } from "../../providers/User";
import { Logo, Container, UserName, Logout } from "./styles";
import { IoLogOutOutline } from "react-icons/io5";

const Header = () => {
  const navigate = useNavigate();
  const { user, setAuth } = useUser();

  const handleLogout = () => {
    setAuth("");
    localStorage.removeItem("@shuffle:token");
    navigate("/");
  };
  return (
    <Container>
      <UserName>{user.name}</UserName>
      <Logo src="https://i.imgur.com/hYjS2es.png" alt="ShuffleLogo" onClick={()=> navigate('/dashboard')}/>
      <Logout onClick={handleLogout}>
        Logout <IoLogOutOutline />
      </Logout>
    </Container>
  );
};

export default Header;
