import styled from "styled-components";

export const Maincontainer = styled.div`
  background: black;
  width: 100vw;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Contentcontainer = styled.div`
  width: 1332px;
  height: 624px;
  display: flex;
  justify-content: space-evenly;
`;

export const Line = styled.div`
  background: white;
  width: 3px;
  position: relative;
  top: 18px;
  border-radius: 1000px;
  height: 585px;
`;

export const LogoContainer = styled.div`
  border-radius: 100%;
  width: 130px;
  top: 306px;
  background-color: white;
  display: flex;
  justify-content: center;
  height: 130px;
  position: absolute;
  align-items: center;

  .logo {
    width: 100px;
  }
`;
