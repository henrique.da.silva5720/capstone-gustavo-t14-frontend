import CharsDisplay from "../../components/CharsDisplay";
import GroupsDisplay from "../../components/groupsDisplay";
import Header from "../../components/header";
import { Container } from "./styles";

const DashboardPage = () => {
  return (
    <Container>
      <Header />
      <GroupsDisplay />
      <CharsDisplay />
    </Container>
  );
};

export default DashboardPage;
