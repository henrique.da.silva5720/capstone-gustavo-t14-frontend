import { Routes, Route } from "react-router-dom";

import RequireAuth from "./routes";

import HomePage from "../pages/Home";
import DashboardPage from "../pages/Dashboard";
import CharacterSheetPage from "./../pages/CharacterSheet";
import Header from "../components/header";

const Router = () => {
  return (
		<Routes>
			<Route path="/" element={<HomePage />} />
			<Route
				path="/dashboard"
				element={
					<RequireAuth>
						<DashboardPage />
					</RequireAuth>
				}
			/>
			<Route
				path="/character_sheet/:id"
				element={
					<RequireAuth>
						<>
							<Header />
							<CharacterSheetPage />
						</>
					</RequireAuth>
				}
			/>
		</Routes>
  );
};

export default Router;
