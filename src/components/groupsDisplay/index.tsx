import { Container, Title } from "./styles";
import { GiThunderBlade } from "react-icons/gi";

const GroupsDisplay = () => {
  return (
    <Container>
      <Title>Grupos</Title>
      <h3>
        Em construção... <GiThunderBlade />
      </h3>
    </Container>
  );
};

export default GroupsDisplay;
