import styled from "styled-components";

export const Maincontainer = styled.div`
  background: black;
  width: 100vw;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Title = styled.h1`
  color: white;
  text-align: center;
  margin: 25px;
`;

export const SignupContainer = styled.div`
  width: 419px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

export const InputBlock = styled.div`
  background: white;
  height: 46px;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 142px;
  border-radius: 5px;
  input {
    border: none;
    width: 122px;
    border-radius: 5px;
  }
`;
export const Block = styled.div`
  margin-top: 10px;
  width: 392px;
  height: 100px;
  display: flex;
  flex-flow: row;
  justify-content: space-evenly;
`;

export const LabelBox = styled.div`
  width: 364px;
  color: white;
  font-size: 18px;
  .unique {
    margin-left: 6px;
  }
`;

export const UniqueLabelBox = styled.div`
  color: white;
`;

export const UniqueBlock = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  width: 192px;
  .password {
    width: 169px;
    height: 42px;
    border: none;
    border-radius: 5px;
    padding-left: 13px;
  }

  .conpassword {
    width: 169px;
    height: 42px;
    padding-left: 13px;
    border: none;
    border-radius: 5px;
  }

  .lPass {
    color: white;
    width: 167px;
    font-size: 18px;
  }

  .CPass {
    color: white;
    font-size: 18px;
  }
`;

export const FormBlock = styled.form`
  width: 392px;
  height: 434px;
  display: flex;
  flex-flow: column;
  align-items: center;

  .username {
    padding: 10px;
    border: none;
    border-radius: 5px;
    width: 367px;
  }

  .email {
    padding: 10px;
    border: none;
    border-radius: 5px;
    width: 367px;
  }
`;

export const BlueButton = styled.button`
  background: #0066ff;
  color: white;
  width: 200px;
  height: 50px;
  font-size: 22px;

  border: none;
  border-radius: 5px;
`;
