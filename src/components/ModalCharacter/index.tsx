import { Background, Container, Form, CloseIcon } from "./styles";

import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

import { useCharacter } from "./../../providers/Character";

import { ICharacterData } from "./../../types";

import { CgCloseO } from 'react-icons/cg'

interface IModalCharacterProps {
  closeModal: React.Dispatch<React.SetStateAction<boolean>>;
}

const ModalCharacter = ({ closeModal }: IModalCharacterProps) => {
  const { createCharacter } = useCharacter();

  const formSchema = yup.object().shape({
    name: yup.string().required("Required field"),
    age: yup
      .number()
      .transform((value, originalValue) =>
        originalValue === "" ? undefined : value
      ),
    gender: yup
      .string()
      .transform((value, originalValue) =>
        originalValue === "" ? undefined : value
      ),
    occupation: yup
      .string()
      .transform((value, originalValue) =>
        originalValue === "" ? undefined : value
      ),
    max_life: yup.string().required("Required field"),
    life: yup.string().required("Required field"),
    max_mana: yup.string().required("Required field"),
    mana: yup.string().required("Required field"),
    height: yup
      .number()
      .transform((value, originalValue) =>
        originalValue === "" ? undefined : value
      ),
    body: yup
      .number()
      .transform((value, originalValue) =>
        originalValue === "" ? undefined : value
      ),
    moviment: yup
      .number()
      .transform((value, originalValue) =>
        originalValue === "" ? undefined : value
      ),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({ resolver: yupResolver(formSchema) });

  const handleForm = (data: ICharacterData) => {
    data.max_life = Number(data.max_life);
    data.life = Number(data.life);
    data.max_mana = Number(data.max_mana);
    data.mana = Number(data.mana);

    createCharacter(data);

    closeModal(false);
  };

  return (
		<Background>
			<Container>
				<h2>Create Character <CloseIcon onClick={()=> closeModal(false)}> <CgCloseO/> </CloseIcon></h2>
				<Form onSubmit={handleSubmit(handleForm)}>
					<label>
						Name *{" "}
						{!!errors.name && <span>- {errors.name?.message}</span>}
					</label>
					<input {...register("name")} />
					<label>
						Age{" "}
						{!!errors.age && <span>- {errors.age?.message}</span>}
					</label>
					<input type="number" {...register("age")} />
					<label>
						Gender{" "}
						{!!errors.gender && (
							<span>- {errors.gender?.message}</span>
						)}
					</label>
					<input {...register("gender")} />
					<label>
						Occupation
						{!!errors.occupation && (
							<span> - {errors.occupation?.message}</span>
						)}
					</label>
					<input {...register("occupation")} />
					<div>
						<div>
							<label>
								Max Life *
								{!!errors.max_life && (
									<span> - {errors.max_life?.message}</span>
								)}
							</label>
							<input type="number" {...register("max_life")} />
						</div>
						<div>
							<label>
								Life *{" "}
								{!!errors.life && (
									<span>- {errors.life?.message}</span>
								)}
							</label>
							<input type="number" {...register("life")} />
						</div>
					</div>
					<div>
						<div>
							<label>
								Max Mana *
								{!!errors.max_mana && (
									<span> - {errors.max_mana?.message}</span>
								)}
							</label>
							<input type="number" {...register("max_mana")} />
						</div>
						<div>
							<label>
								Mana *{" "}
								{!!errors.mana && (
									<span>- {errors.mana?.message}</span>
								)}
							</label>
							<input type="number" {...register("mana")} />
						</div>
					</div>
					<label>
						Height{" "}
						{!!errors.height && (
							<span>- {errors.height?.message}</span>
						)}
					</label>
					<input type="number" step="0.01" {...register("height")} />
					<label>
						Body{" "}
						{!!errors.body && <span>- {errors.body?.message}</span>}
					</label>
					<input type="number" {...register("body")} />
					<label>
						Moviment
						{!!errors.moviment && (
							<span> - {errors.moviment?.message}</span>
						)}
					</label>
					<input type="number" {...register("moviment")} />
					<button type="submit">Create</button>
				</Form>
			</Container>
		</Background>
  );
};

export default ModalCharacter;
