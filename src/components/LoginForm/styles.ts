import styled from "styled-components";

export const LoginContainer = styled.div`
  width: 419px;
`;

export const Title = styled.h1`
  color: white;
  text-align: center;
  margin: 103px 0 20px;
`;

export const LabelBox = styled.div`
  width: 340px;
  color: white;
  font-size: 18px;
  .unique {
    margin-left: 6px;
  }
`;

export const LoginFormBlock = styled.form`
  width: 392px;
  height: 434px;
  display: flex;
  flex-flow: column;
  align-items: center;

  .Lemail {
    padding: 10px;
    border: none;
    border-radius: 5px;
    width: 367px;
  }

  .lpassword {
    padding: 10px;
    border: none;
    border-radius: 5px;
    width: 367px;
  }
`;

export const LBlueButton = styled.button`
  background: #0066ff;
  color: white;
  position: relative;
  width: 200px;
  height: 50px;
  font-size: 22px;
  top: 35px;
  border: none;
  border-radius: 5px;
`;

export const LogoContainer = styled.div`
  border-radius: 100%;
  width: 100px;
  top: 306px;
  display: flex;
  justify-content: center;
  height: 100px;
  position: absolute;
  align-items: center;

  .logo {
    width: 101%;
    height: 97%;
  }
`;
