import {
	FatherContainer,
	Title,
	FormBlock,
	LabelBox,
	InputField,
	CreateButton,
	Background,
	CloseContainer,
} from "./styles";

import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { useSkill } from "../../providers/Skill";
import { useParams } from "react-router-dom";
import { CgCloseO } from "react-icons/cg";

interface IModalCharacterProps {
  closeModal: React.Dispatch<React.SetStateAction<boolean>>;
}

const SkillForm = ({ closeModal }: IModalCharacterProps) => {
	type FormValues = {
		charId: number;
		name: string;
	};

	const params = useParams();

	const { createSkill } = useSkill();

	const formSchema = yup.object().shape({
		name: yup
			.string()
			.required("Nome obrigatorio")
			.matches(/^[a-zA-Z-0-9\s]+$/, "Digite um nome com apenas letras"),
	});

	const {
		register,
		handleSubmit,
		formState: { errors },
	} = useForm({
		resolver: yupResolver(formSchema),
	});

	const numberId = Number(params.id);

	const create = (data: FormValues) => {
		const { name } = data;

		createSkill(numberId, name);
    closeModal(false);
	};

	return (
		<Background>
			<FatherContainer>
				<Title>Create a Skill <CloseContainer onClick={() => closeModal(false)}><CgCloseO /></CloseContainer></Title>
				<FormBlock onSubmit={handleSubmit(create)}>
					<LabelBox>skill name:</LabelBox>
					<InputField {...register("name")} />
					{errors.name?.message}

					<CreateButton type="submit">Create</CreateButton>
				</FormBlock>
			</FatherContainer>
		</Background>
	);
};

export default SkillForm;
