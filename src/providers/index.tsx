import { IProvidersProps } from "./../types";

import { CharacterProvider } from "./Character";
import { SkillProvider } from "./Skill";
import { UserProvider } from "./User";
import { ItemProvider } from "./Items";
import { AttributesProvider } from "./Attributes";

const Providers = ({ children }: IProvidersProps) => {
  return (
    <UserProvider>
      <CharacterProvider>
        <AttributesProvider>
          <SkillProvider>
            <ItemProvider>{children}</ItemProvider>
          </SkillProvider>
        </AttributesProvider>
      </CharacterProvider>
    </UserProvider>
  );
};

export default Providers;
