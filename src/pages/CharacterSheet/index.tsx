import {
	Container,
	ContainerCharacter,
	ContainerStatus,
	ContainerLife,
	Life,
	ContainerMana,
	Mana,
	ContainerAppearance,
	Appearance,
	ContainerData,
	Data,
	ContainerAttributes,
	ContainerSkills,
	ContainerCombat,
	ContainerInventory,
	Item,
	SkillButton,
	AddItemBtn,
	ItemsContainer,
	Weapon,
	WeaponInfos,
	ItemInfos,
	TableTitle,
	TableDel,
	ItemTableTitle,
} from "./styles";

import AttributeSkillContainer from "./../../components/AttributeSkillContainer";

import { useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";

import { useUser } from "./../../providers/User";
import { useCharacter } from "./../../providers/Character";
import ModalItems from "../../components/ModalItems";
import SkillForm from "../../components/CreateSkillForm"
import { TiPlusOutline } from "react-icons/ti";
import ModalStatus from "../../components/ModalStatus";
import { RiDeleteBin5Line } from "react-icons/ri";
import { useItem } from "../../providers/Items";

const CharacterSheetPage = () => {
  const { user } = useUser();

  const { character, readCharacter } = useCharacter();
  const { deleteItem } = useItem();
  const navigate = useNavigate();

  const params = useParams();

  const [showCreateItem, setShowCreateItem] = useState(false);
  const [showCreateSkill, setShowCreateSkill] = useState(false);
  const [showUpdateStatus, setShowUpdateStatus] = useState(false);

  useEffect(() => {
    if (!character.id) {
      readCharacter(Number(params.id));
    }
  });

  

  return (
		<>
			{showCreateItem && (
				<ModalItems
					closeModal={setShowCreateItem}
					char_id={character.id}
				/>
			)}
			{showCreateSkill && <SkillForm closeModal={setShowCreateSkill} />}
			{showUpdateStatus && (
				<ModalStatus closeModal={setShowUpdateStatus} />
			)}
			<Container>
				<div>
					<div>
						<ContainerCharacter>
							<h1>Character Profile</h1>
							<div>
								<figure>
									<img
										src={character.image_url}
										alt="char_img"
									/>
								</figure>
								<div>
									<ContainerStatus>
										<span>Life</span>
										<ContainerLife
											onClick={() =>
												setShowUpdateStatus(true)
											}
										>
											<span>
												{character.life} /{" "}
												{character.max_life}
											</span>
											<Life
												max_life={character.max_life}
												life={character.life}
											/>
										</ContainerLife>
										<span>Mana</span>
										<ContainerMana
											onClick={() =>
												setShowUpdateStatus(true)
											}
										>
											<span>
												{character.mana} /{" "}
												{character.max_mana}
											</span>
											<Mana
												max_mana={character.max_mana}
												mana={character.mana}
											/>
										</ContainerMana>
									</ContainerStatus>
									<ContainerAppearance>
										<Appearance>
											<span>Height</span>
											<span>{character.height}m</span>
											<hr />
										</Appearance>
										<Appearance>
											<span>Body</span>
											<span>{character.body}</span>
											<hr />
										</Appearance>
										<Appearance>
											<span>Moviment</span>
											<span>{character.moviment}m</span>
											<hr />
										</Appearance>
									</ContainerAppearance>
									<ContainerData>
										<Data>
											<span>Player</span>
											<span>{user.name}</span>
											<hr />
										</Data>
										<Data>
											<span>Name</span>
											<span>{character.name}</span>
											<hr />
										</Data>
										<Data>
											<span>Age</span>
											<span>{character.age}</span>
											<hr />
										</Data>
										<Data>
											<span>Gender</span>
											<span>
												{character.gender === "M"
													? "Male"
													: character.gender === "F"
													? "Female"
													: "Other"}
											</span>
											<hr />
										</Data>
										<Data>
											<span>Occupation</span>
											<span>{character.occupation}</span>
											<hr />
										</Data>
									</ContainerData>
								</div>
							</div>
						</ContainerCharacter>
					</div>
					<div>
						<ContainerAttributes>
							<h2>Attributes</h2>
							<div>
								{character.attributes &&
									character.attributes.map(
										(attribute, index) => (
											<AttributeSkillContainer
												key={index}
												obj={attribute}
												character_id={character.id}
											/>
										)
									)}
							</div>
						</ContainerAttributes>
						<ContainerSkills>
							<h2>Skills</h2>
							<SkillButton
								onClick={() => {
									setShowCreateSkill(true);
								}}
							>
								<TiPlusOutline />
								Criar Skill
							</SkillButton>
							<div>
								{character.skills &&
									character.skills.map((skill, index) => (
										<AttributeSkillContainer
											key={index}
											skill
											obj={skill}
											character_id={character.id}
										/>
									))}
							</div>
						</ContainerSkills>
					</div>
				</div>
				<ItemsContainer>
					<ContainerCombat>
						<h2>Combate</h2>
						<div style={{ position: "relative" }}>
							<TableDel></TableDel>
							<TableTitle>Nome</TableTitle>
							<TableTitle>Tipo</TableTitle>
							<TableTitle>Dano</TableTitle>
							<TableTitle>Mun. Atual</TableTitle>
							<TableTitle>Mun. Máx.</TableTitle>
							<TableTitle>Ataques</TableTitle>
							<TableTitle>Alcance</TableTitle>
							<TableTitle>Defeito</TableTitle>
							<TableTitle>Area</TableTitle>
							<AddItemBtn onClick={() => setShowCreateItem(true)}>
								<TiPlusOutline /> Criar arma
							</AddItemBtn>
						</div>
						<hr />
						{character.items &&
							character.items.map((item) => {
								if (item.type === "Combat") {
									return (
										<Weapon>
											<TableDel onClick={() => {
                        deleteItem(item.id, character.id);
                      }}>
                        <RiDeleteBin5Line />
                      </TableDel>
                      <WeaponInfos>
												{item.name}
											</WeaponInfos>
											<WeaponInfos>
												{item.type}
											</WeaponInfos>
											<WeaponInfos>
												{item.damage || "-"}
											</WeaponInfos>
											<WeaponInfos>
												{item.max_ammo || "-"}
											</WeaponInfos>
											<WeaponInfos>
												{item.ammo || "-"}
											</WeaponInfos>
											<WeaponInfos>
												{item.attacks || "-"}
											</WeaponInfos>
											<WeaponInfos>
												{item.range || "-"}
											</WeaponInfos>
											<WeaponInfos>
												{item.defect || "-"}
											</WeaponInfos>
											<WeaponInfos>
												{item.area || "-"}
											</WeaponInfos>
										</Weapon>
									);
								}
								return null;
							})}
					</ContainerCombat>
					<ContainerInventory>
						<h2>Items</h2>
						<div style={{ position: "relative" }}>
							<TableDel>

              </TableDel>
              <ItemTableTitle>Name</ItemTableTitle>
							<ItemTableTitle>Description</ItemTableTitle>
							<AddItemBtn onClick={() => setShowCreateItem(true)}>
								<TiPlusOutline /> Criar item
							</AddItemBtn>
						</div>
						<hr />
						{character.items &&
							character.items.map((item) => {
								if (item.type !== "Combat") {
									return (
										<Item>
											<TableDel
												onClick={() => {
													deleteItem(
														item.id,
														character.id
													);
												}}
											>
												<RiDeleteBin5Line />
											</TableDel>
											<ItemInfos>{item.name}</ItemInfos>
											<ItemInfos>
												{item.description}
											</ItemInfos>
										</Item>
									);
								}
								return null;
							})}
					</ContainerInventory>
				</ItemsContainer>
			</Container>
		</>
  );
};

export default CharacterSheetPage;
