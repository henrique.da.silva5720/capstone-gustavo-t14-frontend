import styled from "styled-components";

export const Background = styled.div`
	width: 100%;
	height: 100%;
	display: flex;
  
	justify-content: center;
	align-items: center;

	background-color: rgba(0, 0, 0, 0.5);
	backdrop-filter: blur(4px);

	position: fixed;
	z-index: 3;
	top: 0;
  left: 0;
`;

export const FatherContainer = styled.div`
	/* width: 579px;
  position: absolute;
  height: 598px;
  top: 10%;
  left: 32%;
  display: flex;
  flex-direction: column;
  align-items: center; */
	width: 500px;

	padding: 20px;

	background-color: var(--black);
	border: 2px solid var(--white);
	border-radius: 10px;
`;

export const Title = styled.h1`
  color: white;
  margin: 10px 0 37px;
  position: relative;
  text-align: center;
`;

export const FormBlock = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const LabelBox = styled.label`
  color: white;
  font-size: 19px;
  width: 280px;
`;

export const InputField = styled.input`
  width: 291px;
  padding: 10px;
  border: none;
  border-radius: 5px;
  margin-bottom: 26px;
`;

export const CreateButton = styled.button`
  background: #0066ff;
  color: white;
  width: 200px;
  height: 50px;
  font-size: 22px;
  border: none;
  border-radius: 5px;
`;

export const CloseContainer = styled.div`
  position: absolute;
  top: -0.5rem;
  right: 0;
  cursor: pointer;
`
