import {
  LoginContainer,
  Title,
  LoginFormBlock,
  LabelBox,
  LBlueButton,
} from "./styles";

import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { useUser } from "../../providers/User";
import { useNavigate } from "react-router-dom";
export const LoginForm = () => {
  const { login } = useUser();
  const navigate = useNavigate();

  type FormValues = {
    email: string;
    password: string;
  };
  const formSchema = yup.object().shape({
    email: yup
      .string()
      .required("email obrigatorio")
      .matches(
        /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
        "formato de email invalido"
      ),
    password: yup
      .string()
      .required("Campo obrigatório")
      .min(8, "Minimo 8 caractéres")
      .matches(
        /^.*((?=.*[!@#$%^&*]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/,
        "Requer: letra maiúscula, minúscula, número, caracter especial"
      ),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(formSchema),
  });

  const user_login = (data: FormValues) => {
    const { email, password } = data;
    const new_object = { email: email, password: password };
    login(new_object, navigate);
  };
  return (
    <>
      <LoginContainer>
        <Title>Log In</Title>
        <LoginFormBlock onSubmit={handleSubmit(user_login)}>
          <LabelBox>Email:</LabelBox>
          <input {...register("email")} className="Lemail" />
          {errors.email?.message}
          <LabelBox>Password:</LabelBox>
          <input {...register("password")} className="lpassword" />
          {errors.password?.message}
          <LBlueButton type="submit">Submit</LBlueButton>
        </LoginFormBlock>
      </LoginContainer>
    </>
  );
};
