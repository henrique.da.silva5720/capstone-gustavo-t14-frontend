import styled from "styled-components";

export const Container = styled.div`
  width: 110px;
  text-align: center;
`;

export const Title = styled.p`
  font-size: 1rem;
`;

export const Line = styled.hr`
  margin: 0 auto 10px auto;
  width: 100px;
`;

export const NumberStyle = styled.p`
  font-size: 2.5rem;
  cursor: pointer;
`;

export const Dice = styled.img`
  width: 25px;
  cursor: pointer;
  @keyframes dice-spin {
    from {
      transform: rotate(0deg);
    }
    to {
      transform: rotate(360deg);
    }
  }
  &:hover {
    animation: dice-spin infinite 1s linear;
  }
`;

export const Change = styled.input`
  width: 60%;
  height: 23px;
  text-align: center;
  font-size: 20px;
  margin: 16px 0px;
  border: none;
  border-radius: 10px;
`;
