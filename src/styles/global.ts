import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`
  :root {
    --white: #ffffff;
    --black: #000000;
    --gray: #808080;
    --red-dark: #800000;
    --red: #a10000;
    --red-light: red;
    --blue-dark: #000090;
    --blue: #0500bb;
    --blue-light: #0066FF;
  }

  * {
    margin: 0;
    padding: 0;
    outline: none;
    box-sizing: border-box;
    list-style-type: none;
    font-family: 'Alegreya SC', serif;
  }

  body {
    color: var(--white);
    background-color: var(--black);
  }

  a, button {
    cursor: pointer;
  }
`;
