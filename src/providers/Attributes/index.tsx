import { createContext, useState, useContext } from "react";
import toast from "react-hot-toast";
import { IProvidersProps } from "../../types";
import { useCharacter } from "../Character";
import api from "./../../services/api";

interface IAttributesData {
  constitution?: number;
  power?: number;
  strength?: number;
  dexterity?: number;
  intelligence?: number;
  luck?: number;
  appearance?: number;
  education?: number;
}

interface IAttributes {
  id: number;
  constitution: number;
  power: number;
  strength: number;
  dexterity: number;
  intelligence: number;
  luck: number;
  appearance: number;
  education: number;
}

interface IAttributesProviderData {
  attributes: IAttributes;
  updateAttributes: (attributesData: IAttributesData, charId: number) => void;
}

const AttributesContext = createContext<IAttributesProviderData>(
  {} as IAttributesProviderData
);

export const AttributesProvider = ({ children }: IProvidersProps) => {
  const token = localStorage.getItem("@shuffle:token") || "";
  const [attributes, setAttributes] = useState<IAttributes>({} as IAttributes);
  const { readCharacter } = useCharacter();

  const updateAttributes = (
    attributesData: IAttributesData,
    charId: number
  ) => {
    api
      .patch(`/attribute/${charId}`, attributesData, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((response) => {
        setAttributes(response.data["attributes"]);
        readCharacter(charId);
        toast.success("Atributos atualizados!");
      })
      .catch((err) => {
        toast.error(err.response.data.error);
      });
  };

  return (
    <AttributesContext.Provider
      value={{
        attributes,
        updateAttributes,
      }}
    >
      {children}
    </AttributesContext.Provider>
  );
};

export const useAttributes = () => useContext(AttributesContext);
